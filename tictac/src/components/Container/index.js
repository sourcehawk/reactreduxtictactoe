import PropTypes from 'prop-types';

const pro_colors = ["#eeeeee", "#00adb5", "#222831", "#393e46"];
const layer_text_colors = ["rgba(0,0,0, 0.75)", "#eeeeee", "#eeeeee", "#eeeeee"];

const Container = ( {children, layer, styleCLS} ) => (
    <div className={ styleCLS? "custom-container "+styleCLS : "custom-container" }
         style={{'backgroundColor': pro_colors[layer], 'color': layer_text_colors[layer]}}>
        { children }
    </div>

);

Container.propTypes = {
    // Defines what layer in the application the container is. Affects the color of the container
    layer: PropTypes.number.isRequired,
    // Add custom style to component
    styleCLS: PropTypes.string
}

export default Container;