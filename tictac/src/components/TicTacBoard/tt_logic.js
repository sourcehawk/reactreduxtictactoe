
function addHV(move, game_counter, isX){
    // do you like bacon?
    move = move-1;
    let h_idx = Math.floor(move/3)
    let v_idx = move - (h_idx*3);
    if (isX) {
        game_counter.horizontal[h_idx]++
        game_counter.vertical[v_idx]++;
    }
    else {
        game_counter.horizontal[h_idx]--;
        game_counter.vertical[v_idx]--;
    }
    game_counter.total++;
    if (game_counter.total === 9) game_counter.isDraw = true;
    if (game_counter.horizontal[h_idx] === 3 || game_counter.horizontal[h_idx] === -3 ||
        game_counter.vertical[v_idx] === 3 || game_counter.vertical[v_idx] === -3)
        game_counter.isWin = true;
}
function addDLR(move, game_counter, isX){
    // Couldn't be arsed to find the correct formula so I've hardcoded it:P
    if (move === 7 || move === 5 || move === 3)
    {
        isX ? game_counter.diagonalLR++ : game_counter.diagonalLR--;
        if (game_counter.diagonalLR=== 3 || game_counter.diagonalLR === -3) game_counter.isWin = true;
    }
    if (move === 1 || move === 5 || move === 9)
    {
        isX ? game_counter.diagonalRL++ : game_counter.diagonalRL--;
        if (game_counter.diagonalRL === 3 || game_counter.diagonalRL === -3) game_counter.isWin = true;
    }
}

export const add_move = (move, game_counter, userSymbol) =>{
    let new_game_counter = {...game_counter}; // because state is const
    let isX = userSymbol === "X";
    addHV(move, new_game_counter, isX);
    addDLR(move, new_game_counter, isX);
    return new_game_counter
}