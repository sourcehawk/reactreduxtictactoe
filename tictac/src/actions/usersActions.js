import {ADD_USER, DISCONNECTED_USER, LOAD_USERS, REMOVE_USER} from "../constants";

export const loadUsers = ( userList ) => {
    return {
        type: LOAD_USERS,
        payload: userList
    }
}

export const addUser = ( user ) => {
    return {
        type: ADD_USER,
        payload: user,
    }
}

export const disconnectedUser = ( userID ) => {
    return {
        type: DISCONNECTED_USER,
        payload: userID
    }
}

export const removeUser = ( userID ) => {
    return {
        type: REMOVE_USER,
        payload: userID,
    }
}