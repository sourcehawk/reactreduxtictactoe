# Tic Tackity Toe
**Author:** _Ægir Máni Hauksson, aegir19@ru.is_\
This project was made as part of a final assignment in web design.
The server was supplemented, the implementation is under the folder _tictac_.

## React Redux
This project makes heavy use of react redux and a custom middleware for socket emission.

## Images
![image](images/demotictactoe.PNG)

![image](images/gameover.PNG)